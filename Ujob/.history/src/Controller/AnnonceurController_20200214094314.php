<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
*  @Route("/api")
*/
class AnnonceurController extends AbstractFOSRestController
{

/**
* @Route("/inscriptionannonceur", name="inscriptionannonceu", methods={"POST"})
*/
public function addannonceur(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder): Response
{
    
                $entreprise= new Entreprise();
                $form = $this->createForm(EntrepriseType::class, $entreprise);
                $data=$request->request->all();
                $form->submit($data);
                $entreprise->setLogo("dienne.png");
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($entreprise);
                $entityManager->flush();

    $utilisateur = new User();
    $form=$this->createForm(UserType::class , $utilisateur);
    $form->handleRequest($request);
    $data=$request->request->all();
    $form->submit($data);

    $utilisateur->setRoles(["ROLE_ANNONCEUR"]);
    $utilisateur->setStatut("connecter");
    $utilisateur->setPhoto("image.png");
    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur,
    $form->get('password')->getData()
        )
        );
    $entityManager = $this->getDoctrine()->getManager();
     $utilisateur->setEntreprise($entreprise);
    $entityManager->persist($utilisateur);
    $entityManager->flush();
    return new Response('Un Annonceur est bien inscrit',Response::HTTP_CREATED); 
}
/**
* @Route("/deconnecter/user/{id}", name="deconnecter_user", methods={"GET"})
* @IsGranted({"ROLE_SUPERADMIN","ROLE_ADMIN"}, statusCode=403, message="Vous n'avez pas accès à cette page !")
*/ 
    public function bloqueUser(UserInterface $Userconnecte,ObjectManager $manager, User $user=null)
    {
        
        if(!$user){
            throw new HttpException(404,'Cet utilisateur n\'existe pas !');
        }
        if($user==$Userconnecte){
            throw new HttpException(403,'Impossible de se bloquer soit même !');
        }
        $entreprise=$user->getEntreprise(); 
        if($Userconnecte->getEntreprise()!=$entreprise){//si un super admin et caissier sont dans la meme entreprises les admin principaux les admins et les users simple aussi
            throw new HttpException(403,'Impossible de bloquer cet utilisateur !');
        }
        elseif($user->getId()==1){
            throw new HttpException(403,'Impossible de bloquer le super-admin !');
        }
        if($Userconnecte->getRoles()[0]=='ROLE_ADMIN' && $user->getRoles()[0]=='ROLE_SUPERADMIN'){
            throw new HttpException(403,'Impossible de bloquer l\' admin principal !');
        }
        
        if($user->getStatut() == $this->connecter){
            $user->setStatut($this->deconnecter);
            $texte=$this->deconnecter;
        }
        else{
            $user->setStatut($this->connecter);
            $texte= 'Déconnecter';
        }
        $manager->persist($user);
        $manager->flush();
        $afficher = [ $this->statut => 200, $this->message => $texte];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
    }
}
