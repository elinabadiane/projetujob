<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
*  @Route("/api")
*/
class AnnonceurController extends AbstractFOSRestController
{

/**
* @Route("/inscriptionannonceur", name="inscriptionannonceu", methods={"POST"})
*/
public function addannonceur(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder): Response
{
    
                $entreprise= new Entreprise();
                $form = $this->createForm(EntrepriseType::class, $entreprise);
                $data=$request->request->all();
                $form->submit($data);
                $entreprise->setLogo("dienne.png");
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($entreprise);
                $entityManager->flush();

    $utilisateur = new User();
    $form=$this->createForm(UserType::class , $utilisateur);
    $form->handleRequest($request);
    $data=$request->request->all();
    $form->submit($data);

    $utilisateur->setRoles(["ROLE_ANNONCEUR"]);
    $utilisateur->setStatut("connecter");
    $utilisateur->setPhoto("image.png");
    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur,
    $form->get('password')->getData()
        )
        );
    $entityManager = $this->getDoctrine()->getManager();
     $utilisateur->setEntreprise($entreprise);
    $entityManager->persist($utilisateur);
    $entityManager->flush();
    return new Response('Un Annonceur est bien inscrit',Response::HTTP_CREATED); 
}


    /**
    * @Route("/deconnecteannonceur/{id}", name="deconnecteannonceur", methods={"GET"})
    * @IsGranted({"ROLE_SUPERADMIN"}, statutCode=403, message="Vous n'avez pas accès à cette page !")
    */ 
    public function bloqueEntrep(ObjectManager $manager,User $entreprise=null)
    {
        if(!$entreprise){
            throw new HttpException(404,'Ce annonceur n\'existe pas !!');
        }
        elseif($entreprise->getUsername()=='contact@hakimdigitalsa.coml'){
            throw new HttpException(403,'Impossible de de le Déconnecter !');
        }
        elseif($entreprise->getStatut() == $this->connecter){
            $entreprise->setStatut($this->deconnecterStr);
            $texte= 'Annonceur déconnecter';
        }
        else{
            $entreprise->setStatut($this->connecter);
            $texte= 'Annonceur déconnecter';
        }
        $manager->persist($entreprise);
        $manager->flush();
        $afficher = [ $this->status => 200, $this->message => $texte];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
    }
}
