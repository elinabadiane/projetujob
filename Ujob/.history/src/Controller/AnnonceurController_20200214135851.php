<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
*  @Route("/api")
*/
class AnnonceurController extends FOSRestController 
{

    private $connecter;
    private $deconnecter;

    public function __construct()
    {
        $this -> connecter ="Connecter";
        $this -> deconnecter ="Deconnecter"; 
    }
  
/**
* @Route("/inscriptionannonceur", name="inscriptionannonceu", methods={"POST"})
*/
public function addannonceur(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder): Response
{

    
                $entreprise= new Entreprise();
                $form = $this->createForm(EntrepriseType::class, $entreprise);
                $data=$request->request->all();
                $form->submit($data);
                $entreprise->setLogo("dienne.png");
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($entreprise);
                $entityManager->flush();

    $utilisateur = new User();
    $form=$this->createForm(UserType::class , $utilisateur);
    $form->handleRequest($request);
    $data=$request->request->all();
    $form->submit($data);

    $utilisateur->setRoles(["ROLE_ANNONCEUR"]);
    $utilisateur->setStatut($this->connecter);
    $utilisateur->setPhoto("image.png");
    $utilisateur->setPassword($passwordEncoder->encodePassword($utilisateur,
    $form->get('password')->getData()
        )
        );
    $entityManager = $this->getDoctrine()->getManager();
     $utilisateur->setEntreprise($entreprise);
    $entityManager->persist($utilisateur);
    $entityManager->flush();
    return new Response('Un Annonceur est bien inscrit',Response::HTTP_CREATED); 
}


/**
* @Route("/annonceur/bloquer/{id}", name="bloquer_debloquer_annonceur", methods={"GET"})
*/ 
    public function bloqueAnnonceur(ObjectManager $manager,User $utilisateur=null)
    {
        if($utilisateur->getStatut() == $this->connecter){
            $utilisateur->setStatut($this->deconnecter);
            $texte= 'Annonceur déconnecter';
        }
        else{
            $utilisateur->setStatut($this->connecter);
            $texte='Annonceur connecter';
        }
        $manager->persist($utilisateur);
        $manager->flush();
        $afficher = [ $this->status => 200, $this->message => $texte];
        return $this->handleView($this->view($afficher,Response::HTTP_OK));
    }
}
